# -*- coding: utf-8 -*-
from unittest import TestCase
from os import linesep

import spaces_and_tabs as sat

# python -m unittest discover -v

class Sat(TestCase):

    def setUp(self):

        self.TAB_DOUBLE_SPACE_CATCHME = "{0}  Catch me if you can".format(chr(9))
        self.TAB_DOUBLE_SPACE_CATCHME2 = "  {0}Catch me2 if you can".format(chr(9))
        self.TAB_DOUBLE_SPACE_IGNOREME_NONLEADING = "  Good {0}  Still Good".format(chr(9))

        self.TESTSCRIPT1 = "".join(r"""#!/bin/bash
        printf "\n"
        exit
        """)

        # See input/tab_then_doublespace.sh for a more involved shell script example for input
        return


    def test_spaces_and_tabs_report1(self):
        satres = sat.spaces_and_tabs_report1(self.TAB_DOUBLE_SPACE_CATCHME)
        self.assertTrue(satres)
        satres = sat.spaces_and_tabs_report1(self.TAB_DOUBLE_SPACE_CATCHME2)
        self.assertTrue(satres)
        satres = sat.spaces_and_tabs_report1(
            self.TAB_DOUBLE_SPACE_IGNOREME_NONLEADING)
        self.assertFalse(satres)


    def test_spaces_and_tabs_report(self):
        lines_matched_count = sat.spaces_and_tabs_report([self.TESTSCRIPT1,""])
        self.assertEqual(lines_matched_count,0)

