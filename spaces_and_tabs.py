#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

""" Report about lines that contain leading tabs intermingled with spaces """
""" Tested against Python 3.2.3 """

# python -m unittest discover -v

import fileinput
import re
from os import path
from string import printable
from sys import argv,exit

""" set_printable = set(printable) """

re_wordchar = re.compile('\w')
re_tab_then_wordchar = re.compile('\t\w')
""" re_spaces_two_or_more = re.compile('\s\s+\w')
The regex '\s\s+\w' would probably not do what you require as 
\s is equivalent to the class [ \t\n\r\f\v]
"""
re_spaces_two_or_more = re.compile(' {2,}\w')
re_tab_then_space_then_wordchar = re.compile('\t {1,}\w')
re_space_then_tab_then_wordchar = re.compile(' {1,}\t\w')
re_hash = re.compile('#')


def spaces_and_tabs_mixed(line):
    if re_wordchar.match(line):
        return False
    elif re_hash.match(line):
        return False
    elif re_tab_then_wordchar.match(line):
        return False
    elif re_spaces_two_or_more.match(line):
        """ re_spaces_two_or_more_then_wordchar might be a better name.
        Nothing wrong with a line that is caught by this match. All good. """
        #print("match102:",end='')
        #print(line)
        return False
    elif re_tab_then_space_then_wordchar.match(line):
        """ tab and spaces mixed """
        #print("match900:",end='')
        #print(line)
        satres = True
    elif re_space_then_tab_then_wordchar.match(line):
        """ spaces and tab mixed """
        #print("match1029:",end='')
        #print(line)
        satres = True
    else:
        satres = False
    #print("spaces_and_tabs_mixed() returning {0}".format(satres))
    return satres


def print_line_output(line_input, line_count=None):
    if line_count is None:
        line_output = "{0} and begins {1:10}".format(
            'Line has tabs and spaces',line_input[:10])
    else:
        line_output = "Line {0:4d} has {1}".format(
            line_count,'tabs and spaces')
    print(line_output)


def spaces_and_tabs_report1(line):
    """ Return True if line passes all match() so found """
    satres = False
    if spaces_and_tabs_mixed(line):
        satres = True
        print_line_output(line)
    return satres


def spaces_and_tabs_report(lines):
    """ Return count of lines passed all match() so found """
    lines_matched = 0
    counter = 0
    for line in lines:
        counter += 1
        if spaces_and_tabs_mixed(line):
            lines_matched += 1
            print_line_output(line,counter)
    return lines_matched


def spaces_and_tabs_report_all():
    """ Return count of lines passed all match() so found """
    lines_matched = 0
    counter = 0
    for line in fileinput.input():
        counter += 1
        if spaces_and_tabs_mixed(line):
            lines_matched += 1
            print_line_output(line,counter)
    return lines_matched


if __name__ == "__main__":
    spaces_and_tabs_report_all()

