#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

""" Replace each [leading] block of N spaces with tab character
where N is 4 by default but supplied as first argument if 2 or 8 is preferred.

Uses fileinput so for single file you can use standard in like this:
cat some.code | python3 ./tabify_all 2    # Here we are saying 2 spaces gets tab

For single tabify you could instead just use M-x tabify in emacs
"""

""" Tested against Python 3.2.3 """

# python -m unittest discover -v

import fileinput
import re
from os import getenv as osgetenv
from os import linesep
from string import printable
from sys import argv,exit

""" set_printable = set(printable) """

SPACER=chr(32)
HASHER=chr(35)
COLON=chr(58)
EQUALS=chr(61)
TABBY=chr(9)


re_wordchar = re.compile('\w')
re_tab_then_wordchar = re.compile("{0}\w".format(TABBY))
""" re_spaces_two_or_more = re.compile('\s\s+\w')
The regex '\s\s+\w' would probably not do what you require as
\s is equivalent to the class [ \t\n\r\f\v]
"""
re_spaces_two_or_more = re.compile(SPACER+"{2,}\w")
re_tab_then_space_then_wordchar = re.compile('\t {1,}\w')
re_space_then_tab_then_wordchar = re.compile(' {1,}\t\w')
re_hasher = re.compile(HASHER)
re_space_equals_space = re.compile("{0}{1}{0}".format(SPACER,EQUALS))
SPACENUM_DEFAULT=4


def spaces_and_tabs_mixed(line):
	if re_wordchar.match(line):
		return False
	elif re_hash.match(line):
		return False
	elif re_tab_then_wordchar.match(line):
		return False
	elif re_spaces_two_or_more.match(line):
		""" re_spaces_two_or_more_then_wordchar might be a better name.
		Nothing wrong with a line that is caught by this match. All good. """
		#print("match102:",end='')
		#print(line)
		return False
	elif re_tab_then_space_then_wordchar.match(line):
		""" tab and spaces mixed """
		#print("match900:",end='')
		#print(line)
		satres = True
	elif re_space_then_tab_then_wordchar.match(line):
		""" spaces and tab mixed """
		#print("match1029:",end='')
		#print(line)
		satres = True
	else:
		satres = False
	#print("spaces_and_tabs_mixed() returning {0}".format(satres))
	return satres


def print_line_output(line_input, line_count=None):
	if line_count is None:
		line_output = "{0} and begins {1:10}".format(
			'Line has tabs and spaces',line_input[:10])
	else:
		line_output = "Line {0:4d} has {1}".format(
			line_count,'tabs and spaces')
	print(line_output)


def spaces_and_tabs_report1(line):
	""" Return True if line passes all match() so found """
	satres = False
	if spaces_and_tabs_mixed(line):
		satres = True
		print_line_output(line)
	return satres


def spaces_and_tabs_report(lines):
	""" Return count of lines passed all match() so found """
	lines_matched = 0
	counter = 0
	for line in lines:
		counter += 1
		if spaces_and_tabs_mixed(line):
			lines_matched += 1
			print_line_output(line,counter)
	return lines_matched


def process_fileinput(fileinput_given,number_of_spaces=SPACENUM_DEFAULT):
	""" Iterate and prepare details of suitable spacing blocks by
	returning a list of 4 tuples,
	for non-blank lines where,... first item is human indexed line number
	and last item (of the tuple) is the original line
	"""

	list_of_tuples = []
	if number_of_spaces < 2:
		# Single spacing to tabs not currently implemented
		return list_of_tuples

	idx = 0
	for line in fileinput_given.input():
		idx += 1
		if len(line.strip()) < 1:
			# Lines of all blanks not a consideration
			continue
		lstripped_len = len(line.lstrip(SPACER))
		spaces_len = len(line)-lstripped_len
		rem_spacing = spaces_len%number_of_spaces
		whole_block_spacing = (spaces_len-rem_spacing)//number_of_spaces
		""" if whole_block_spacing < 1: continue
	if we did the above continue line then we would not have a full record
	of all non-blank lines to later reconstruct.
	"""
		tup4 = (idx,whole_block_spacing,rem_spacing,line)
		list_of_tuples.append(tup4)

	return list_of_tuples


def tabify_lines(list_of_tuples,replacechar=TABBY,verbosity=0):
	""" Make suitable replacements using supplied replacechar
	based on figures and contents stored in the supplied
	list_of_tuples
	Giving None for replacechar is asking for a file that is
	very similar or identical to the input file!
	"""
	lines = []
	idx0 = 0
	for tup in tup4list:
		idxhum = tup[0]
		insert_blankline_count = (idxhum-(idx0+1))
		if insert_blankline_count > 0:
			#print(insert_blankline_count)
			for i in range(insert_blankline_count):
				idx0+=1
				lines.append('')
				continue
		if replacechar is None:
			line = tup[3]
			lines.append(line.rstrip())
		else:
			# Make a suitable replacement
			space_blocks = tup[1]
			space_rem = tup[2]
			line = tup[3]
			if space_blocks > 0:
				line_new = space_blocks*replacechar
				spaces = space_rem*SPACER
				# Doing a line.lstrip() is not sufficient next
				line_new = "{0}{1}{2}".format(line_new,
							spaces,line.strip())
			else:
				line_new = line.rstrip()
			lines.append(line_new)
		idx0+=1
	# Full iteration of [re-constructed] file complete

	""" Verbosity 2 or greater says print the lines
	aswell as returning them from the function.
	"""
	if verbosity > 1:
		for line in lines:
			print(line)

	return lines

if __name__ == "__main__":

	program_binary = argv[0].strip()
	from os import path
	program_dirname = path.dirname(program_binary)

	PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
	PYVERBOSITY = 1
	if PYVERBOSITY_STRING is None:
		pass
	else:
		try:
			PYVERBOSITY = int(PYVERBOSITY_STRING)
		except:
			pass
	#print(PYVERBOSITY)

	""" Next verbosity_global = 1 is the most likely outcome
	unless the user specifically overrides it by setting
	the environment variable PYVERBOSITY
	"""
	if PYVERBOSITY is None:
		verbosity_global = 1
	elif 0 == PYVERBOSITY:
		# 0 from env means 0 at global level
		verbosity_global = 0
	elif PYVERBOSITY > 1:
		# >1 from env means same at global level
		verbosity_global = PYVERBOSITY
	else:
		verbosity_global = 1

	spacenum = 0
	spacenum_str = None
	try:
		spacenum_str = argv[1]
	except IndexError:
		spacenum = -1

	if spacenum_str is None:
		# Using default assumption spacenum=4 as no user request
		pass
	else:
		try:
			spacenum = int(spacenum_str)
		except:
			spacenum = -2

	fileinput2process = None
	if 0 == spacenum:
		# Perhaps user supplied 0 but in any case use default
		spacenum = SPACENUM_DEFAULT
	elif -1 == spacenum:
		# No argument for spacenum supplied
		spacenum = SPACENUM_DEFAULT
	elif -2 == spacenum:
		# Likely that first arg is a filepath
		spacenum = SPACENUM_DEFAULT
		fileinput2process = argv[2:]
	else:
		# Fall through for use default
		spacenum = SPACENUM_DEFAULT

	if fileinput2process is None:
		""" If not already decided on index based
		assignment then give fileinput2process
		the whole thing
		"""
		fileinput2process = fileinput

	""" Our assumption is that blank lines can be ignored and later
	re-inserted appended with os.linesep
	Function tabify_lines does any printing and re-inserting.
	"""

	tup4list = process_fileinput(fileinput2process,spacenum)

	# Supplying 2 as final arg next instructs function to print also
	lines = tabify_lines(tup4list,TABBY,2)
