#!/bin/sh
if [ 0 -lt 1 ]; then
    printf "Mathematically well behaved.\n"
else
    printf "Mathematical anomaly?.\n"
fi
# Above is default emacs23 (spaces)
# Below were created with a printf "\t" >> to force tabs
# Below have tabs before the printf !
if [ 0 -lt 1 ]; then
	printf "Math well behaved.\n"
else
	printf "Math anomaly?.\n"
fi
# Next have tab then two spaces before the printf !
	  printf "In Mathematics sometimes formatting is important.\n"
# Next have two spaces then tab before the printf !
  	printf "In Mathematics sometimes formatting is important (again).\n"
exit
